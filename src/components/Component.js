export default class Component {
  tagName;
  attribute;
  children;
  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.attribute = attribute;
    this.children = children;
  }

  renderAttribute() {
    return `<${this.tagName} ${this.attribute.name}=\"${this.attribute.value}\" />`;
  }

  renderChildren() {
    if (this.children instanceof Array) {
      let tab = "";
      if (this.attribute) tab = `${tab}${this.renderAttribute()}`;

      this.children.forEach((element) => {
        console.log(element instanceof Component);
        if (element instanceof Component) {
          tab = `${tab}${element.render()}`;
        } else if (element instanceof Array) {
          tab = `${tab}${element.renderChildren()}`;
        } else {
          tab = `${tab}${element}`;
        }
      });
      return `<${this.tagName}>${tab}</${this.tagName}>`;
    }
    return `<${this.tagName}>${this.children}</${this.tagName}>`;
  }
  render() {
    if (this.children) return this.renderChildren();
    return this.renderAttribute();
  }
}
