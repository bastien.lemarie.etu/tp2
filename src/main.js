import data from "./data.js";
import Router from "./Router.js";
import Component from "./components/Component.js";
import Img from "./components/Img.js";
import PizzaThumbnail from "./components/PizzaThumbnail.js";
import PizzaList from "./pages/PizzaList.js";

/*
const title = new Component("h1", "", "La carte");
document.querySelector(".pageTitle").innerHTML = title.render();

const img = new Img(
  "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300"
);
document.querySelector(".pageContent").innerHTML = img.render();

const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector(".pageContent").innerHTML = pizzaThumbnail.render();
*/
Router.titleElement = document.querySelector(".pageTitle");
Router.contentElement = document.querySelector(".pageContent");

const pizzaList = new PizzaList([]);
Router.routes = [{ path: "/", page: pizzaList, title: "La carte" }];

Router.navigate("/"); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate("/"); // affiche la liste des pizzas
